
var submit = document.getElementsByTagName("input")[2];
var table = document.getElementById("pixelCanvas");
// Select color input
var colorPicker = document.getElementById("colorPicker");
// Select size input
var height = document.getElementById("inputHeight");
var width = document.getElementById("inputWidth");
// When size is submitted by the user, call makeGrid()
function makeGrid() {
// Your code goes here!
	//CLEAR THE TABLE
  for(var i = table.rows.length -1; i >= 0; i--)
	{
   	 table.deleteRow(i);
	}
  	//store the selected color value
	var selectedColor = colorPicker.value;
 	var a, b,  rowElem, colElem;
    a = height.value;
    b = width.value;

    if (a == "" || b == "") {
        alert("Please enter some numeric value");
    } else {
        for (var i = 0; i < a; i++) {
            rowElem = document.createElement('tr');
            for (var j = 0; j < b; j++) {
                colElem = document.createElement('td');
                //colElem.appendChild(document.createTextNode(j + 1)); //to print cell number
                rowElem.appendChild(colElem);
            }
            table.appendChild(rowElem);
        }
        document.body.appendChild(table);
    }
}
submit.addEventListener("click",makeGrid);

// Event Listener for each cell
table.addEventListener("click",function(event){
	color= colorPicker.value;
	let element = event.target; //returns the clicked element inside list
	element.style.backgroundColor = color;

});


